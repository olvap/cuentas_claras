import { defineStore } from 'pinia'

export const store = defineStore('store', {
  state: () => ({
    friends: [
      {
        name: 'Pablo',
        amount: 100,
      },
      {
        name: 'Ivan',
        amount: 200,
      },
    ],
    extras: 0,
  }),
  // adding suport for localstorage
  // https://github.com/prazdevs/pinia-plugin-persistedstate
  persist: true,

  getters: {
    totalMoney: (state) => {
      //convertir esto con un reduce
      let sum = 0
      state.friends.forEach((friend) => {
        sum += friend.amount
      })
      return sum
    },
    partySize: (state) => state.friends.length + state.extras,
  },
  actions: {
    reset() {
      this.friends = [{ name: '', amount: 0 }]
    },
    addFriend() {
      console.log(this.friends)
      this.friends.push({ name: '', amount: 0 })
    },
    removeFriend(index) {
      this.friends.splice(index, 1)
    },
    updateAmount(index, amount) {
      this.friends[index].amount = parseFloat(amount)
    },
    updateName(index, name) {
      this.friends[index].name = name
    },
    increase_extras() {
      this.extras++
    },
    decrease_extras() {
      if (this.extras <= 0) {
        return 0
      }

      this.extras--
    },
  },
})
